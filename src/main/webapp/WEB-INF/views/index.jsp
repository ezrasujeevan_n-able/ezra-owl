<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="<c:url value="/resources/bootstrap-4.0.0-b/css/bootstrap.css"/>" rel="stylesheet"
          type="text/css"/>


</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-2">
            <span class="oi oi-icon-name" title="icon name" aria-hidden="true"></span>
            <span class="oi" data-glyph="icon-name" title="icon name" aria-hidden="true"></span>
        </div>
        <div class="col-md-8">
            <h1>Student Details</h1>
            <a class="btn btn-primary" href="<spring:url value="/edit"/> " role="button">Add</a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>First Name <span class="oi oi-icon-name" title="icon name" aria-hidden="true"></span></th>
                    <th>Last Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${students}" var="student">

                    <tr>
                        <td>${student.firstName}</td>
                        <td>${student.lastName}</td>
                        <td><a href="<spring:url value="/edit?id=${student.id}"/> ">Edit<span
                                class="glyphicon glyphicon-pencil"></span></a></td>
                        <td><a href="<spring:url value="/delete?id=${student.id}"/> ">Delete<span
                                class="glyphicon glyphicon-remove"></span></a></td>

                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<c:url value="/resources/jquery-3.2.1/jquery-3.2.1.js"/>"
        type="text/javascript"></script>
<script src="<c:url value="/resources/popper.js-1.12.3/popper.js"/>"
        type="text/javascript"></script>
<script src="<c:url value="/resources/bootstrap-4.0.0-b/js/bootstrap.js"/>"
        type="text/javascript"></script>

</body>
</html>