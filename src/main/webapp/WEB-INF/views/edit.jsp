<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core_1_1" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="<c:url value="/resources/bootstrap-4.0.0-b/css/bootstrap.css"/>" rel="stylesheet"
          type="text/css"/>


</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            <h1>Student Details Edit</h1>

            <form:form action="${pageContext.request.contextPath}/edit"
                       method="post" commandName="student" modelAttribute="student">
                <div class="form-group row">
                    <label for="id" class="col-sm-3 col-form-label">ID</label>
                    <div class="col-sm-9">
                        <form:input type="text" readonly="true" class="form-control" path="id"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="firstName" class="col-sm-3 col-form-label">First Name</label>
                    <div class="col-sm-9">
                        <form:input type="text" class="form-control" id="staticEmail" path="firstName"
                                    placeholder="John"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="lastName" class="col-sm-3 col-form-label">Last Name</label>
                    <div class="col-sm-9">
                        <form:input type="text" class="form-control" id="staticEmail" path="lastName"
                                    placeholder="Doe"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="addressLine1" class="col-sm-3 col-form-label">Address Line 1</label>
                    <div class="col-sm-9">
                        <form:input type="text" class="form-control" id="staticEmail" path="addressLine1"
                                    placeholder="Doe"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="addressLine2" class="col-sm-3 col-form-label">Address Line 2</label>
                    <div class="col-sm-9">
                        <form:input type="text" class="form-control" id="staticEmail" path="addressLine2"
                                    placeholder="Doe"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="city" class="col-sm-3 col-form-label">City</label>
                    <div class="col-sm-9">
                        <form:input type="text" class="form-control" id="staticEmail" path="city"
                                    placeholder="Doe"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="state" class="col-sm-3 col-form-label">State</label>
                    <div class="col-sm-9">
                        <form:input type="text" class="form-control" id="staticEmail" path="state"
                                    placeholder="Doe"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="country" class="col-sm-3 col-form-label">Country</label>
                    <div class="col-sm-9">
                        <form:input type="text" class="form-control" id="staticEmail" path="country"
                                    placeholder="Doe"/>
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-sm-6">
                        <a class="btn btn-outline-primary" href="<spring:url value="/"/>" role="button">Cancel</a>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-primary" type="submit">Save</button>
                    </div>
                </div>

            </form:form>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<c:url value="/resources/jquery-3.2.1/jquery-3.2.1.js"/>"
        type="text/javascript"></script>
<script src="<c:url value="/resources/popper.js-1.12.3/popper.js"/>"
        type="text/javascript"></script>
<script src="<c:url value="/resources/bootstrap-4.0.0-b/js/bootstrap.js"/>"
        type="text/javascript"></script>
</body>
</html>