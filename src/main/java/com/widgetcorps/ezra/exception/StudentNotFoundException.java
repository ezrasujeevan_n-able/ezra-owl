package com.widgetcorps.ezra.exception;

public class StudentNotFoundException extends RuntimeException {
    public StudentNotFoundException(String id) {
        super(String.format("No Student entry found with id: <%s>", id));
    }
}
