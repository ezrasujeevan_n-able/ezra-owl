package com.widgetcorps.ezra.dao;

import com.widgetcorps.ezra.model.StudentModel;

import java.util.List;

public interface StudentDao {

    void saveOrUpdateStudent(StudentModel studentModel);

    void deleteStudent(int id);

    StudentModel getStudentById(int id);

    List<StudentModel> getAllStudents();

}
