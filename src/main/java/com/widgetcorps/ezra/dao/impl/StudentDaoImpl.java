package com.widgetcorps.ezra.dao.impl;

import com.widgetcorps.ezra.dao.StudentDao;
import com.widgetcorps.ezra.model.StudentModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class StudentDaoImpl implements StudentDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void saveOrUpdateStudent(StudentModel studentModel) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(studentModel);
        session.flush();

    }

    @Override
    public void deleteStudent(int id) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(getStudentById(id));
        session.flush();
    }

    @Override
    public StudentModel getStudentById(int id) {
        Session session = sessionFactory.getCurrentSession();
        StudentModel studentModel = (StudentModel) session.get(StudentModel.class, id);
        session.flush();
        return studentModel;
    }

    @Override
    public List<StudentModel> getAllStudents() {
        Session session = sessionFactory.getCurrentSession();
        List<StudentModel> studentModels = session.createCriteria(StudentModel.class).list();
        session.flush();
        return studentModels;
    }
}
