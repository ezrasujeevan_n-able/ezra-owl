package com.widgetcorps.ezra.dao;

import com.widgetcorps.ezra.model.StudentModel;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends Repository<StudentModel, String> {

    void delete(StudentModel deleted);

    List<StudentModel> findAll();

    Optional<StudentModel> findOne(String id);

    StudentModel save(StudentModel saved);
}

