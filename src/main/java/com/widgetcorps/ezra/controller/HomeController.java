package com.widgetcorps.ezra.controller;

import com.widgetcorps.ezra.dao.StudentDao;
import com.widgetcorps.ezra.model.StudentModel;
import com.widgetcorps.ezra.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class HomeController {

    private final StudentService service;
    @Autowired
    StudentDao studentDao;

    @Autowired
    HomeController(StudentService service) {
        this.service = service;
    }

    @RequestMapping(path = "/")
    public String index(Model model) {

        List<StudentModel> studentModels = service.findAll();
        model.addAttribute("students", studentModels);

        return "index";
    }

    @RequestMapping(path = "/edit", method = RequestMethod.GET)
    public String edit(Model model, @RequestParam(value = "id", required = false) String id) {
        StudentModel studentModel;
        if (id == null || id.isEmpty()) {
            studentModel = new StudentModel();
        } else {
            studentModel = service.findById(id);
        }
        model.addAttribute("student", studentModel);
        return "edit";
    }

    @RequestMapping(path = "/edit", method = RequestMethod.POST)
    public String save(@ModelAttribute(name = "studentModel", value = "studentModel") StudentModel studentModel, Model model) {
        System.out.println(studentModel);
        if (studentModel.getId().isEmpty()) {
            studentModel.setId(null);
            service.create(studentModel);
        } else {
            service.update(studentModel);
        }
//        studentDao.saveOrUpdateStudent(studentModel);
        return "redirect:/";
    }

    @RequestMapping(path = "/delete")
    public String delete(Model model, @RequestParam(value = "id") String id) {
//        studentDao.deleteStudent(id);
        service.delete(id);
        return "redirect:/";
    }
}
