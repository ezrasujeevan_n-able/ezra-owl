package com.widgetcorps.ezra.service.mongo;

import com.widgetcorps.ezra.dao.StudentRepository;
import com.widgetcorps.ezra.exception.StudentNotFoundException;
import com.widgetcorps.ezra.model.StudentModel;
import com.widgetcorps.ezra.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;

    @Autowired
    StudentServiceImpl(StudentRepository repository) {
        this.repository = repository;
    }

    @Override
    public StudentModel create(StudentModel studentModel) {
        studentModel = repository.save(studentModel);
        return studentModel;
    }

    @Override
    public StudentModel delete(String id) {
        StudentModel studentModel = findStudentById(id);
        repository.delete(studentModel);
        return studentModel;
    }

    @Override
    public List<StudentModel> findAll() {
        List<StudentModel> studentModelList = repository.findAll();
        return studentModelList;
    }

    @Override
    public StudentModel findById(String id) {
        StudentModel studentModel = findStudentById(id);
        return studentModel;
    }

    @Override
    public StudentModel update(StudentModel studentModel) {
        StudentModel updated = findStudentById(studentModel.getId());
        updated.update(studentModel);
        updated = repository.save(updated);
        return updated;
    }

    private StudentModel findStudentById(String id) {
        Optional<StudentModel> result = repository.findOne(id);
        return result.orElseThrow(() -> new StudentNotFoundException(id));

    }
}
