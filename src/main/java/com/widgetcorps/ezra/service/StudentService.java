package com.widgetcorps.ezra.service;
import com.widgetcorps.ezra.model.StudentModel;

import java.util.List;

public interface StudentService {

    StudentModel create(StudentModel studentModel);

    StudentModel delete(String id);

    List<StudentModel> findAll();

    StudentModel findById(String id);

    StudentModel update(StudentModel StudentModel);
}
